import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: any = {};
  hide: boolean = true;
  
  constructor(
    public api: ApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }
  login()
  {
    this.api.login(this.user.email, this.user.password).subscribe(res=>{
      localStorage.setItem('appToken',JSON.stringify(res));
      this.router.navigate(['admin/dashboard']);
    },err=>{
      alert('tidak bisa login');
    });
  }
}
